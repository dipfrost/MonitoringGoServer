FROM golang


COPY ./server.go /go

RUN cd /go \
  && go get github.com/go-sql-driver/mysql \
  && go get github.com/gin-gonic/gin \
  && go build ./server.go


CMD ["./server"]
