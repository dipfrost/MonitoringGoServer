package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

var db, err = sql.Open("mysql", "userr:password@tcp(db:3306)/mydb")

func main() {
	router := gin.Default()
	//router.GET("/api/containers/:id", getById)

	router.POST("/api/containers", container)
	router.POST("/api/clients", clients)
	router.POST("/api/docker", docker)
	router.POST("/api/wallets", wallets)

	router.Run()

}

//-----------------------------------------------------------------------------begin containers part-------------------------------------------

func container(c *gin.Context) {
	Container := struct {
		NodeName     string `json:"nodename"`
		DockerName   string `json:"dockername"`
		Blocks       string `json:"blocks"`
		Status       string `json:"status"`
		Version      string `json:"version"`
		Vin          string `json:"vin"`
		ActiveStatus string `json:"active_status"`
		Timestamp    string `json:"timestamp"`
		Locate       string `json:"locate"`
		NodeNumber   string `json:"dockernumber"`
		Genkey       string `json:"genkey"`
	}{}

	if c.BindJSON(&Container) == nil {
		fmt.Println("json matched!!!!!!!")
	} else {
		fmt.Println("json not matched!!!!!!!")
	}

	if isContainerAlreadyExist(Container.DockerName, Container.Locate) > 0 {
		containerUpdate(isContainerAlreadyExist(Container.DockerName, Container.Locate), Container.NodeName, Container.DockerName, Container.Blocks, Container.Status, Container.Version, Container.Vin, Container.ActiveStatus, Container.Timestamp, Container.Locate, Container.NodeNumber, Container.Genkey)
		c.JSON(200, gin.H{
			"method":  "updatecontainer",
			"message": Container,
		})
		fmt.Println(Container)
	} else {
		containerAdd(Container.NodeName, Container.DockerName, Container.Blocks, Container.Status, Container.Version, Container.Vin, Container.ActiveStatus, Container.Timestamp, Container.Locate, Container.NodeNumber, Container.Genkey)
		c.JSON(200, gin.H{
			"method":  "addcontainer",
			"message": Container,
		})
		fmt.Println(Container)
	}

}

func isContainerAlreadyExist(DockerName string, Locate string) int {
	var ID int
	err := db.QueryRow("SELECT id FROM containers WHERE dockername= ? and locate= ? ", DockerName, Locate).Scan(&ID)
	switch {
	case err == sql.ErrNoRows:
		return 0
	case err != nil:
		log.Fatal(err)
		return 0
	default:
		return ID
	}
}

func containerAdd(NodeName string, DockerName string, Blocks string, Status string, Version string, Vin string, ActiveStatus string,
	Timestamp string, Locate string, DockerNumber string, Genkey string) {

	stmt, err := db.Prepare("insert into containers (nodename, dockername, blocks, status, version, vin, active_status, timestamp, locate, dockernumber, genkey) values(?,?,?,?,?,?,?,?,?,?,?);")
	if err != nil {
		fmt.Print(err.Error())
	}

	_, err = stmt.Exec(NodeName, DockerName, Blocks, Status, Version, Vin, ActiveStatus, Timestamp, Locate, DockerNumber, Genkey)
	if err != nil {
		fmt.Print(err.Error())
	}

	defer stmt.Close()

}

func containerUpdate(ID int, NodeName string, DockerName string, Blocks string, Status string, Version string, Vin string, ActiveStatus string,
	Timestamp string, Locate string, DockerNumber string, Genkey string) {

	stmt, err := db.Prepare("update containers set nodename= ?, dockername= ?, blocks= ?, status= ?, version= ?, vin= ?,  active_status= ?, Timestamp = ?, locate = ?, dockernumber = ?, genkey = ?  where id= ?;")
	if err != nil {
		fmt.Print(err.Error())
	}

	_, err = stmt.Exec(NodeName, DockerName, Blocks, Status, Version, Vin, ActiveStatus, Timestamp, Locate, DockerNumber, Genkey, ID)
	if err != nil {
		fmt.Print(err.Error())
	}

	defer stmt.Close()

}

func containerDelete(Name string) {
	stmt, err := db.Prepare("delete from container where name = ?;")
	if err != nil {
		fmt.Print(err.Error())
	}

	_, err = stmt.Exec(Name)
	if err != nil {
		fmt.Print(err.Error())
	}

	defer stmt.Close()
}

//-----------------------------------------------------------------------------end containers part------------------------------------------

//-----------------------------------------------------------------------------begin clients part-------------------------------------------

func clients(c *gin.Context) {
	Clients := struct {
		IP   string `json:"ip"`
		Name string `json:"name"`
		Port string `json:"port"`
	}{}

	c.BindJSON(&Clients)

	if isClientAlreadyExist(Clients.Name) > 0 {
		clientsUpdate(Clients.Name, Clients.IP, Clients.Port, isClientAlreadyExist(Clients.Name))
		c.JSON(http.StatusOK, gin.H{
			"new":     "0",
			"message": Clients,
		})
		fmt.Println(Clients)
	} else {
		clientsAdd(Clients.Name, Clients.IP, Clients.Port)
		c.JSON(http.StatusOK, gin.H{
			"new":     "1",
			"message": Clients,
		})
		fmt.Println(Clients)
	}
}

func isClientAlreadyExist(NamePc string) int {
	var ID int
	err := db.QueryRow("SELECT id FROM clients WHERE name= ? ", NamePc).Scan(&ID)
	switch {
	case err == sql.ErrNoRows:
		return 0
	case err != nil:
		log.Fatal(err)
		return 0
	default:
		return ID
	}
}

func clientsAdd(Name string, IP string, Port string) {
	stmt, err := db.Prepare("insert into clients (name, ip, port) values(?,?,?);")
	if err != nil {
		fmt.Print(err.Error())
	}

	_, err = stmt.Exec(Name, IP, Port)
	if err != nil {
		fmt.Print(err.Error())
	}

	defer stmt.Close()

}

func clientsUpdate(Name string, IP string, Port string, ID int) {
	stmt, err := db.Prepare("update clients set name= ?, ip= ?, port= ?  where id= ?;")
	if err != nil {
		fmt.Print(err.Error())
	}

	_, err = stmt.Exec(Name, IP, Port, ID)
	if err != nil {
		fmt.Print(err.Error())
	}

	defer stmt.Close()

}

func clientsDelete(Name string) {
	stmt, err := db.Prepare("delete from clients where name = ?;")
	if err != nil {
		fmt.Print(err.Error())
	}

	_, err = stmt.Exec(Name)
	if err != nil {
		fmt.Print(err.Error())
	}

	defer stmt.Close()
}

//-----------------------------------------------------------------------------end clients part--------------------------------------------

//-----------------------------------------------------------------------------bergin docker part------------------------------------------

func isDockerImageAlreadyExist(Imageid string) int {
	var ID int
	err := db.QueryRow("SELECT id FROM docker WHERE imageid= ? ", Imageid).Scan(&ID)
	switch {
	case err == sql.ErrNoRows:
		return 0
	case err != nil:
		log.Fatal(err)
		return 0
	default:
		return ID
	}
}

func docker(c *gin.Context) {
	Docker := struct {
		Image    string `json:"image"`
		Tag      string `json:"tag"`
		Locate   string `json:"locate"`
		Imageid  string `json:"imageid"`
		Clientid string `json:"userid"`
	}{}

	c.BindJSON(&Docker)

	if isDockerImageAlreadyExist(Docker.Imageid) > 0 {
		c.JSON(http.StatusOK, gin.H{
			"method":  "dockeradd",
			"message": Docker,
		})
	} else {
		stmt, err := db.Prepare("insert into docker (image, tag, locate, imageid, userid) values(?,?,?,?,?);")
		if err != nil {
			fmt.Print(err.Error())
		}

		_, err = stmt.Exec(Docker.Image, Docker.Tag, Docker.Locate, Docker.Imageid, Docker.Clientid)
		if err != nil {
			fmt.Print(err.Error())
		}

		defer stmt.Close()

		c.JSON(http.StatusOK, gin.H{
			"method":  "dockerupdate",
			"message": Docker,
		})
	}

}

//-----------------------------------------------------------------------------end docker part---------------------------------------------

//-----------------------------------------------------------------------------bergin wallet part------------------------------------------

func walletUpdate(ID int, Name string, IP string, Port string, Timestamp string, Locate string, Daemon string, Cli string,
	ConfDir string, ConfFile string, DockerImage string, Sentinel string, Active string, Work string) {

	stmt, err := db.Prepare("update wallets set name= ?, ip= ?, port= ?, timestamp= ?, locate= ?, daemon= ?,  cli= ?, confdir = ?, conffile = ?, dockerimage = ?, sentinel = ?, active = ?, work = ?  where id= ?;")
	if err != nil {
		fmt.Print(err.Error())
	}

	_, err = stmt.Exec(Name, IP, Port, Timestamp, Locate, Daemon, Cli, ConfDir, ConfFile, DockerImage, Sentinel, Active, Work, ID)
	if err != nil {
		fmt.Print(err.Error())
	}

	defer stmt.Close()

}

func walletAdd(Name string, IP string, Port string, Timestamp string, Locate string, Daemon string, Cli string,
	ConfDir string, ConfFile string, DockerImage string, Sentinel string, Active string, Work string) {

	stmt, err := db.Prepare("insert into wallets (name, ip, port, timestamp, locate, daemon, cli, confdir, conffile, dockerimage, sentinel, active, work) values(?,?,?,?,?,?,?,?,?,?,?,?,?);")
	if err != nil {
		fmt.Print(err.Error())
	}

	_, err = stmt.Exec(Name, IP, Port, Timestamp, Locate, Daemon, Cli, ConfDir, ConfFile, DockerImage, Sentinel, Active, Work)
	if err != nil {
		fmt.Print(err.Error())
	}

	defer stmt.Close()

}

func isWalletAlreadyExist(Imageid string, Locate string) int {
	var ID int
	err := db.QueryRow("SELECT id FROM wallets WHERE name = ? and locate = ? ", Imageid, Locate).Scan(&ID)
	switch {
	case err == sql.ErrNoRows:
		return 0
	case err != nil:
		log.Fatal(err)
		return 0
	default:
		return ID
	}
}

func wallets(c *gin.Context) {
	Wallets := struct {
		Name        string `json:"name"`
		IP          string `json:"ip"`
		Port        string `json:"port"`
		Timestamp   string `json:"timestamp"`
		Locate      string `json:"locate"`
		Daemon      string `json:"daemon"`
		Cli         string `json:"cli"`
		ConfDir     string `json:"confdir"`
		ConfFile    string `json:"conffile"`
		DockerImage string `json:"dockerimage"`
		Sentinel    string `json:"sentinel"`
		Active      string `json:"active"`
		Work        string `json:"work"`
	}{}

	c.BindJSON(&Wallets)

	if isWalletAlreadyExist(Wallets.Name, Wallets.Locate) > 0 {
		walletUpdate(isWalletAlreadyExist(Wallets.Name, Wallets.Locate), Wallets.Name, Wallets.IP, Wallets.Port, Wallets.Timestamp, Wallets.Locate, Wallets.Daemon, Wallets.Cli, Wallets.ConfDir, Wallets.ConfFile, Wallets.DockerImage, Wallets.Sentinel, Wallets.Active, Wallets.Work)
		c.JSON(http.StatusOK, gin.H{
			"method": "walletupdate",
			"name":   Wallets,
		})
	} else {
		walletAdd(Wallets.Name, Wallets.IP, Wallets.Port, Wallets.Timestamp, Wallets.Locate, Wallets.Daemon, Wallets.Cli, Wallets.ConfDir, Wallets.ConfFile, Wallets.DockerImage, Wallets.Sentinel, Wallets.Active, Wallets.Work)
		c.JSON(http.StatusOK, gin.H{
			"method": "walletadd",
			"name":   Wallets,
		})
	}

}

//-----------------------------------------------------------------------------end wallet part----------------------------------------------
